#include <stdlib.h>
#include <stdio.h>

#include <dbus/dbus.h>


// return bool or <0 for error
static int ussdd_callnoarg (const char *method) {
  DBusError err;
  DBusConnection *conn;
  DBusMessage *msg;
  DBusMessageIter args;
  DBusPendingCall *pending;
  int res = 0;
  //
  dbus_error_init(&err);
  //
  // connect to the bus
  conn = dbus_bus_get(DBUS_BUS_SYSTEM, &err);
  if (dbus_error_is_set(&err)) {
    fprintf(stderr, "DBus: Connection Error (%s)\n", err.message);
    dbus_error_free(&err);
    //if (conn != NULL) dbus_connection_close(conn);
    conn = NULL;
  }
  //
  if (conn == NULL) { return -1; }
  //
  msg = dbus_message_new_method_call("su.kibergus.ussdd", "/su/kibergus/ussdd", "su.kibergus.ussdd", method);
  if (msg == NULL) {
    // it's ok -- there is no ussdd
    //fprintf(stderr, "DBus: can't initiate method call\n");
    //dbus_connection_close(conn);
    return -1;
  }
  // arguments
  dbus_message_iter_init_append(msg, &args);
  /*
  if (!dbus_message_iter_append_basic(&args, DBUS_TYPE_STRING, &param)) {
    fprintf(stderr, "Out Of Memory!\n");
    exit(1);
  }
  */
  // send message and get a handle for a reply
  if (!dbus_connection_send_with_reply(conn, msg, &pending, -1)) { // -1 is default timeout
    fprintf(stderr, "DBus: can't call method\n");
    dbus_message_unref(msg);
    //dbus_connection_close(conn);
    return -1;
  }
  //
  if (pending == NULL) {
    fprintf(stderr, "DBus: 'pending' is NULL\n");
    dbus_message_unref(msg);
    //dbus_connection_close(conn);
    return -1;
  }
  dbus_connection_flush(conn);
  // free message
  dbus_message_unref(msg);
  //
  // block until we receive a reply
  dbus_pending_call_block(pending);
   // get the reply message
  msg = dbus_pending_call_steal_reply(pending);
  if (msg == NULL) {
    fprintf(stderr, "DBus: reply is NULL\n");
    dbus_message_unref(msg);
    //dbus_connection_close(conn);
    return -1;
  }
  // free the pending message handle
  dbus_pending_call_unref(pending);
  //
  // read the replies
  // here we have at least bool reply
  if (!dbus_message_iter_init(msg, &args)) {
    //fprintf(stderr, "Message has no arguments!\n");
    res = 0;
  } else if (DBUS_TYPE_BOOLEAN != dbus_message_iter_get_arg_type(&args)) {
    //fprintf(stderr, "Argument is not boolean!\n");
    res = 0;
  } else {
    dbus_message_iter_get_basic(&args, &res);
    //fprintf(stderr, "stat=%d\n", stat);
  }
  //
  /*
  if (!dbus_message_iter_next(&args)) {
    fprintf(stderr, "Message has too few arguments!\n");
  } else if (DBUS_TYPE_UINT32 != dbus_message_iter_get_arg_type(&args)) {
    fprintf(stderr, "Argument is not int!\n");
  } else {
    dbus_uint32_t level;
    //
    dbus_message_iter_get_basic(&args, &level);
    fprintf(stderr, "level=%u\n", level);
  }
  */
  // free reply and close connection
  dbus_message_unref(msg);
  //dbus_connection_close(conn);
  return res;
}


int ussdd_skipnext (void) {
  return ussdd_callnoarg("skip_next");
}


int ussdd_shownext (void) {
  return ussdd_callnoarg("show_next");
}
