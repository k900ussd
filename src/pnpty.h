/*
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef PNPTY_H
#define PNPTY_H

#include <stdarg.h>
#include <stdlib.h>
#include <unistd.h>

#ifdef __cplusplus
extern "C" {
#endif


typedef struct PnPty PnPty;


extern PnPty *pnptyNew (void);
extern void pnptyFree (PnPty *pt);

// return string or NULL on error
// if retunrned string starts with '\1' -- it's modem error
extern char *pnptySendCommandVA (PnPty *pt, const char *fmt, va_list args);
extern char *pnptySendCommand (PnPty *pt, const char *fmt, ...) __attribute__((format(printf,2,3)));

extern int pnptySendSMSCommand (PnPty *pt, const char *number, const char *str);


#ifdef __cplusplus
}
#endif
#endif
