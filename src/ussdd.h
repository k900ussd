/*
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef USSDH_H
#define USSDH_H

#ifdef __cplusplus
extern "C" {
#endif


// both returns bool or <0 on error
extern int ussdd_skipnext (void);
extern int ussdd_shownext (void);


#ifdef __cplusplus
}
#endif
#endif
