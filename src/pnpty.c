/*
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include "pnpty.h"

#include <errno.h>
#include <fcntl.h>
#include <pty.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>

#include <sys/types.h>
#include <sys/wait.h>


//#define dlog(...)  fprintf(stderr, __VA_ARGS)
#define dlog(...)


////////////////////////////////////////////////////////////////////////////////
struct PnPty {
  int fd;
  pid_t pid;
};


////////////////////////////////////////////////////////////////////////////////
static void pnptyKillChild (PnPty *pt) {
  if (pt != NULL && pt->pid > 0) {
    int status;
    //
    if (kill(pt->pid, 0) >= 0) kill(pt->pid, SIGKILL);
    //if (kill(pt->pid, 0) >= 0) kill(pt->pid, SIGTERM);
    /*TODO: wait a little, then kill that stubborn child*/
    if (waitpid(pt->pid, &status, 0/*|WNOHANG*/) >= 0) {
    }
    pt->pid = 0;
  }
}


static void pnptyClear (PnPty *pt) {
  if (pt != NULL) {
    if (pt->fd >= 0) close(pt->fd);
    pt->fd = -1;
    pnptyKillChild(pt);
  }
}


static int setNonBlock (int fd, int doset) {
  int flags;
  //
  if ((flags = fcntl(fd, F_GETFL, 0)) == -1) flags = 0;
  if (doset) flags |= O_NONBLOCK; else flags &= ~O_NONBLOCK;
  if (fcntl(fd, F_SETFL, flags) == -1) return -1;
  //
  return 0;
}


static int ensureModemIsReady (int fd) {
  for (;;) {
    char buf[1024];
    int bufpos;
    //
    if (setNonBlock(fd, 0) < 0) return -1;
    if (write(fd, "AT shit\r", 8) <= 0) return -1;
    // read echo
    for (;;) {
      if (read(fd, buf, 1) != 1) return -1;
      if (buf[0] == '\n') break;
    }
    if (setNonBlock(fd, 1) < 0) return -1;
    //
    // wait for 'ERROR'; 100 ms
    bufpos = 0;
    for (int f = 100; f > 0; --f) {
      int rd = read(fd, buf+bufpos, sizeof(buf)-bufpos-1);
      //
      if (rd < 0) {
        if (errno == EINTR) continue;
        if (errno == EAGAIN) { usleep(1000); continue; }
        // alas
        return -1;
      }
      //
      if (rd > 0) {
        char *eol;
        //
        bufpos += rd;
        buf[bufpos] = 0;
        if ((eol = strchr(buf, '\n')) != NULL) {
          int np = (eol-buf)+1;
          //
          if (eol > buf && eol[-1] == '\r') --eol;
          *eol = 0;
          //fprintf(stderr, "ensure: [%s]\n", buf);
          if (strcmp(buf, "ERROR") == 0) return 0; // ok
          if (np < bufpos) memmove(buf, buf+np, bufpos-np);
          bufpos -= np;
        }
        //
        if (bufpos >= sizeof(buf)-2) return -1; // alas
      }
    }
    // and do it all again
  }
  //
  return 0; // never reached
}


static int pnptyRun (PnPty *pt) {
  if (pt != NULL) {
    // let's prepare args
    char *args[2];
    //
    args[0] = "/usr/bin/pnatd";
    args[1] = NULL;
    //
    if ((pt->pid = forkpty(&pt->fd, NULL, NULL, NULL)) >= 0) {
      if (pt->pid == 0) {
        // child
        execvp(args[0], args);
        exit(99);
      }
      // parent
      return 0;
    } // else -- error
  }
  return -1;
}


////////////////////////////////////////////////////////////////////////////////
void pnptyFree (PnPty *pt) {
  if (pt != NULL) {
    pnptyClear(pt);
    free(pt);
  }
}


PnPty *pnptyNew (void) {
  PnPty *res = malloc(sizeof(PnPty));
  //
  if (res != NULL) {
    res->fd = -1;
    res->pid = 0;
    //
    if (pnptyRun(res) != 0 || ensureModemIsReady(res->fd) != 0) {
      pnptyFree(res);
      return NULL;
    }
  }
  //
  return res;
}


enum {
  TIMEOUT = 40000
};


// remove empty lines (SLOOOW)
static void trimemptylines (char *str) {
  for (char *eol = strrchr(str, '\n'); eol != NULL; eol = strrchr(str, '\n')) {
    if (eol[1]) break; // non-empty line follows
    if (eol > str && eol[-1] == '\r') --eol;
    *eol = 0;
  }
}


// return string or NULL on error
char *pnptySendCommandVA (PnPty *pt, const char *fmt, va_list args) {
  if (pt != NULL && pt->fd >= 0 && pt->pid > 0) {
    //static const char *eol = "\r\n";
    char s[1024], *buf = s;
    int sz = sizeof(s)-4;
    int bufsz = 1024, bufpos = 0;
    int waitcnt = 0, firstline = 1;
    //
    for (;;) {
      va_list ap;
      int n;
      //
      va_copy(ap, args);
      n = vsnprintf(s, sz, fmt, ap);
      va_end(ap);
      //
      if (n > -1 && n < sz) { sz = n; break; } // ok
      if (n < 0) n = sz*2; else ++n;
      //
      if (buf == s) {
        if ((buf = malloc(n+4)) == NULL) return NULL;
      } else {
        char *t;
        //
        if ((t = realloc(buf, n+4)) == NULL) { free(buf); return NULL; }
        buf = t;
      }
      sz = n;
    }
    //
    if (setNonBlock(pt->fd, 0) < 0) {
      if (buf != s) free(buf);
      return NULL;
    }
    //
    dlog("sending: [%s]\n", s);
    strcat(s, "\r");
    sz = write(pt->fd, s, strlen(s));
    if (buf != s) free(buf);
    if (sz <= 0) return NULL;
    //
    if (setNonBlock(pt->fd, 1) < 0) return NULL;
    if ((buf = calloc(bufsz+4, 1)) == NULL) return NULL;
    //
    for (;;) {
      int rd;
      //
      if (bufpos >= bufsz) {
        int newsz = bufsz+1024;
        char *nb = realloc(buf, newsz+4);
        //
        if (nb == NULL) { free(buf); return NULL; }
        memset(buf+bufpos, newsz-bufsz, 0);
        buf = nb;
        bufsz = newsz;
      }
      //
      if ((rd = read(pt->fd, buf+bufpos, 1)) <= 0) {
        const char *msg;
        //
        if (errno == EINTR) continue;
        if (errno == EAGAIN) {
          if (waitcnt++ < TIMEOUT) {
            usleep(1000);
            //sleep(1);
            continue;
          }
        }
        //
        msg = strerror(errno);
        dlog("rd: %d; [%s]\n", rd, msg);
        free(buf);
        fprintf(stderr, "ERROR!\n");
        return NULL;
      }
      if (buf[bufpos] == 0) buf[bufpos] = ' ';
      //dlog("rd=%d; char=%d (%c)\n", rd, (unsigned char)(buf[bufpos]), (buf[bufpos] >= 32 && buf[bufpos] != 128 ? buf[bufpos] : '.'));
      ++bufpos;
      //
      if (buf[bufpos-1] == '\n') {
        char *lastn;
        int err = 0;
        //
        if (firstline) {
          // ignore first line (this is our command echoed)
          firstline = 0;
          bufpos = 0;
          memset(buf, 0, bufsz);
          continue;
        }
        //
        if (bufpos > 1 && buf[bufpos-2] == '\r') buf[(--bufpos)-1] = '\n';
        dlog("===\n%s===\n", buf);
        buf[bufpos-1] = 0;
        if ((lastn = strrchr(buf, '\n')) == NULL) lastn = buf; else ++lastn;
        //
        if (strcmp(lastn, "OK") == 0) {
          if (buf[0] == 1) buf[0] = ' ';
          // remove OK line and last EOL
          if (lastn > buf) {
            lastn[-1] = 0;
            lastn[lastn-2 >= buf && lastn[-2] == '\r' ? -2 : -1] = 0;
          } else {
            buf[0] = 0;
          }
          trimemptylines(buf);
          //dlog("===\n%s===\n", buf);
          break;
        }
        //
        if (lastn[0] == '+' && lastn[1] == 'C' && strlen(lastn) >= 10) {
          char oc = lastn[3];
          //
          lastn[3] = '?';
          err = (strncmp(lastn, "+CM? ERROR", 10) == 0);
          lastn[3] = oc;
        }
        //
        if (err || strcmp(lastn, "ERROR") == 0) {
          //dlog("===\n%s===\n", buf);
          buf[bufpos-1] = '\n';
          memmove(buf+1, buf, strlen(buf)+1);
          buf[0] = '\1';
          trimemptylines(buf);
          break;
        }
        buf[bufpos-1] = '\n';
      }
    }
    //
    //dlog("DONE\n");
    return buf;
  }
  //
  return NULL;
}


__attribute__((format(printf,2,3))) char *pnptySendCommand (PnPty *pt, const char *fmt, ...) {
  va_list ap;
  char *res;
  //
  va_start(ap, fmt);
  res = pnptySendCommandVA(pt, fmt, ap);
  va_end(ap);
  //
  return res;
}


static int skipLine (PnPty *pt, int dump) {
  int waitcnt = 0;
  //
  for (;;) {
    int rd;
    char ch;
    //
    if ((rd = read(pt->fd, &ch, 1)) <= 0) {
      const char *msg;
      //
      if (errno == EINTR) continue;
      if (errno == EAGAIN) {
        if (waitcnt++ < TIMEOUT) { usleep(1000); continue; }
      }
      //
      msg = strerror(errno);
      dlog("rd: %d; [%s]\n", rd, msg);
      fprintf(stderr, "ERROR!\n");
      return -1;
    }
    if (dump) fputc(ch, stderr);
    if (ch == '\n') return 0; // done
  }
}


int pnptySendSMSCommand (PnPty *pt, const char *number, const char *str) {
  if (pt != NULL && pt->fd >= 0 && pt->pid > 0 && number != NULL && number[0] && str != NULL && str[0]) {
    //static const char *eol = "\r";
    static const char *cmd0 = "AT+CMGS=\"";
    static const char *cmd1 = "\"\r";
    static const char *ctrlz = "\x1a";
    //
    int waitcnt = 0;
    char repbuf[64];
    int reppos;
    int sz;
    //
    if (setNonBlock(pt->fd, 0) < 0) return -1;
    //dlog("pnptySendSMSCommand: sending CMGS for '%s'\n", number);
    if ((sz = write(pt->fd, cmd0, strlen(cmd0))) < 0) return -1;
    if ((sz = write(pt->fd, number, strlen(number))) < 0) return -1;
    if ((sz = write(pt->fd, cmd1, strlen(cmd1))) < 0) return -1;
    //
    // now we should receive "> " or some shit (?)
    if (setNonBlock(pt->fd, 1) < 0) return -1;
    //
    if (skipLine(pt, 0) < 0) return -1;
    //dlog("reading '> '...\n");
    reppos = 0;
    while (reppos < 2) {
      int rd;
      //
      if ((rd = read(pt->fd, repbuf+reppos, 1)) <= 0) {
        const char *msg;
        //
        if (errno == EINTR) continue;
        if (errno == EAGAIN) {
          if (waitcnt++ < TIMEOUT) { usleep(1000); continue; }
        }
        //
        msg = strerror(errno);
        dlog("rd: %d; [%s]\n", rd, msg);
        fprintf(stderr, "ERROR!\n");
        return -1;
      }
      ++reppos;
    }
    //
    //dlog("REP: '%c%c'\n", repbuf[0], repbuf[1]);
    //
    if (repbuf[0] != '>' || repbuf[1] != ' ') {
      // invalid answer; read till EOL and return error
      dlog("REP: '%c%c'\n", repbuf[0], repbuf[1]);
      skipLine(pt, 1);
      return -1;
    }
    // now send text
    //dlog("sending text...\n");
    if (setNonBlock(pt->fd, 0) < 0) return -1;
    if ((sz = write(pt->fd, str, strlen(str))) < 0) return -1;
    if ((sz = write(pt->fd, ctrlz, strlen(ctrlz))) < 0) return -1;
    // now read 'OK'
    //dlog("reading reply...\n");
    if (setNonBlock(pt->fd, 1) < 0) return -1;
    if (skipLine(pt, 0) < 0) return -1; // skip echoed str
    reppos = 0;
    for (;;) {
      int rd;
      //
      if ((rd = read(pt->fd, repbuf+reppos, 1)) <= 0) {
        const char *msg;
        //
        if (errno == EINTR) continue;
        if (errno == EAGAIN) {
          if (waitcnt++ < TIMEOUT) { usleep(1000); continue; }
        }
        //
        msg = strerror(errno);
        dlog("rd: %d; [%s]\n", rd, msg);
        fprintf(stderr, "ERROR!\n");
        return -1;
      }
      //dlog("%c (%d)\n", (repbuf[reppos] > ' ' && repbuf[reppos] < 127 ? repbuf[reppos] : '.'), (unsigned int)(repbuf[reppos]));
      // here we should receive "+CMGS: 87" or so
      if (reppos == 0 && repbuf[0] != '+') {
        skipLine(pt, 1);
        return -1;
      }
      /*
      if (reppos == 0 && (repbuf[0] == '\n' || repbuf[0] == '\r')) {
        dlog("%d skipped...\n", repbuf[0]);
        continue; // skip all newlines
      }
      */
      if (repbuf[reppos] == '\n') {
        repbuf[reppos+1] = 0;
        //dlog("SMSREP: %s", repbuf);
        if (strncmp(repbuf, "+CMGS: ", 7) != 0) {
          fprintf(stderr, "SMSERR: %s", repbuf);
          return -1;
        }
        break;
      }
      if (reppos < sizeof(repbuf)-2) ++reppos;
    }
    //dlog("SMS COMPLETE!\n");
    skipLine(pt, 1); // this MUST be "OK"; we should check it, but...
    //
    return 0;
  }
  //
  return -2;
}
