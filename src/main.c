/*
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <ctype.h>
#include <iconv.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "pnpty.h"
#include "ussdd.h"


////////////////////////////////////////////////////////////////////////////////
static const char *skipField (const char *str) {
  const char *e;
  //
  if (str != NULL && str[0]) {
    if (str[0] == '"') {
      //???
      for (e = str+1; *e; ++e) {
        if (*e == '\\') ++e;
        else if (*e == '"') { ++e; break; }
      }
      if (*e && *e != ',') e = NULL; // too bad
    } else {
      e = strchr(str, ',');
      if (e == NULL) e = str+strlen(str); else ++e;
    }
  } else {
    // invalid string or no more fields
    e = NULL;
  }
  //
  return e;
}


static char *getField (const char *str) {
  if (str != NULL && str[0]) {
    char *res;
    //
    if (str[0] == '"') {
      //???
      int len = 0;
      const char *e;
      //
      for (e = str+1; *e; ++e) {
        if (*e == '\\') ++e;
        else if (*e == '"') { ++e; break; }
        ++len;
      }
      if (*e && *e != ',') return NULL; // too bad
      if ((res = malloc(len+1)) != NULL) {
        for (len = 0, e = str+1; *e; ++e, ++len) {
          if (*e == '\\') ++e;
          else if (*e == '"') break;
          res[len] = *e;
        }
        res[len] = 0;
      }
    } else {
      const char *e = strchr(str, ',');
      int len;
      //
      if (e == NULL) e = str+strlen(str);
      len = e-str;
      if ((res = malloc(len+1)) != NULL) {
        if (len > 0) memcpy(res, str, len);
        res[len] = 0;
      }
    }
    //
    return res;
  }
  // error
  return NULL;
}


////////////////////////////////////////////////////////////////////////////////
// parse '+CSQ' command output (signal strength)
// return dBm or -1
//   '113' means '-113 or less', '51' means '-51 or greater', '0' means 'unknown'
// RSSI (dBm) = (-113)+(2*CSQ)
// +CSQ: 15,99
static int parseCSQ (const char *ans) {
  if (ans != NULL && strncmp(ans, "+CSQ: ", 6) == 0 && isdigit(ans[6])) {
    int res = 0;
    //
    ans += 6;
    for (; *ans && isdigit(*ans); ++ans) res = res*10+ans[0]-'0';
    if (*ans != ',') return -1;
    return (res > 31 ? 0 : 113-2*res);
  }
  //
  return -1;
}


////////////////////////////////////////////////////////////////////////////////
static int cmdHelp (PnPty *pd, int argc, char *argv[]);


// +CNUM: ,"+380959320794",145
static int cmdMe (PnPty *pd, int argc, char *argv[]) {
  char *res = pnptySendCommand(pd, "AT+CNUM");
  //
  if (res != NULL) {
    if (strncmp(res, "+CNUM: ", 7) == 0) {
      const char *n = skipField(res+7);
      //
      //printf("[%s]\n", n);
      if (n != NULL && n[0]) {
        char *val = getField(n);
        //
        if (val != NULL) {
          printf("your phone number is %s\n", val);
          free(val);
          free(res);
          //
          return 0;
        }
      }
    }
    free(res);
  }
  //
  fprintf(stderr, "BAD: some error occured.\n");
  return 1; // error
}


// +CSQ: 15,99
static int cmdRSSI (PnPty *pd, int argc, char *argv[]) {
  char *res = pnptySendCommand(pd, "AT+CSQ");
  //
  if (res != NULL) {
    int p = parseCSQ(res);
    //
    if (p >= 0) {
      printf("rssi: ");
      if (p == 0) printf("unknown\n"); else printf("-%d dBm\n", p);
      free(res);
      return 0;
    }
    free(res);
  }
  //
  fprintf(stderr, "BAD: some error occured.\n");
  return 1; // error
}


static int cmdUSSD (PnPty *pd, int argc, char *argv[]) {
  char *res = pnptySendCommand(pd, "AT+CUSD=1,\"%s\",15", argv[1]); // 15: lang=neutral
  //
  if (res != NULL && strncmp(res, "+CUSD: ", 7) == 0) {
    printf("reply: %s\n", res+7);
    free(res);
    return 0;
  } else if (res != NULL) {
    free(res);
  }
  //
  fprintf(stderr, "BAD: some error occured.\n");
  return 1; // error
}


static int cmdBalanceMTS (PnPty *pd, int argc, char *argv[]) {
  char *res = pnptySendCommand(pd, "AT+CUSD=1,\"*101#\",15"); // 15: lang=neutral
  char *val = NULL;
  //
  if (res != NULL && strncmp(res, "+CUSD: ", 7) == 0) {
    const char *str = res+7;
    //
    val = getField(str);
    if (val != NULL && strcmp(val, "0") == 0) {
      free(val);
      str = skipField(str);
      val = getField(str);
      if (val != NULL && (isdigit(val[0]) || val[0] == '.')) {
        int wasdot = 0;
        char *p;
        //
        for (p = val; *p; ++p) {
          if (!isdigit(*p)) {
            if (*p == '.') {
              if (wasdot) { p = NULL; break; }
              wasdot = 1;
              continue;
            }
            //
            if (*p == ' ') {
              if (strncmp(p, " UAH", 4) == 0) p += 4;
              break;
            }
            //
            p = NULL;
            break;
          }
        }
        //
        if (p != NULL) {
          *p = 0;
          printf("balance: %s\n", val);
          free(val);
          free(res);
          return 0;
        }
      }
    }
  }
  //
  if (val != NULL) free(val);
  if (res != NULL) free(res);
  //
  fprintf(stderr, "BAD: some error occured.\n");
  return 1; // error
}


static int cmdCommand (PnPty *pd, int argc, char *argv[]) {
  char *res = pnptySendCommand(pd, "AT%s", argv[1]);
  //
  if (res != NULL) {
    if (res[0] == '\1') {
      printf("=== ERROR ===\n%s\n", res);
    } else {
      printf("=== REPLY ===\n%s\n", res);
    }
    free(res);
    return 0;
  }
  //
  fprintf(stderr, "BAD: some error occured.\n");
  return 1; // error
}


static char *str2hex (const char *encoding, const char *str) {
  iconv_t cd;
  char *outs = NULL, *ibuf, *obuf, *ress;
  size_t il, ol, ool;
  //
  if (str == NULL) return NULL;
  if (encoding == NULL) encoding = "KOI8-U";
  if ((cd = iconv_open("UCS-2BE", encoding)) == (iconv_t)-1) return NULL; // invalid encoding
  ibuf = (char *)str;
  il = strlen(str);
  outs = malloc(il*2+8);
  obuf = outs;
  ool = ol = il*2+2;
  il = iconv(cd, &ibuf, &il, &obuf, &ol);
  if (il == (size_t)-1) goto error;
  //if (reslen) *reslen = ool-ol;
  iconv_close(cd);
  if ((ress = calloc(ool-ol+1, 2)) == NULL) { free(outs); return NULL; }
  for (size_t f = 0; f < ool-ol; ++f) sprintf(ress+f*2, "%02X", (unsigned char)(outs[f]));
  free(outs);
  return ress;
error:
  iconv_close(cd);
  free(outs);
  return NULL;
}


enum {
  MAXSMSLEN = 160-6*2,  // 6 octets for UDHI
  FULLSMSLEN = 160
};


static int cmdSMS (PnPty *pd, int argc, char *argv[]) {
  char *res = NULL;
  int msglen, msgmax;
  char *hexstr;
  int msgid = 42;
  //
  if (!argv[2][0] || !argv[1][0]) { fprintf(stderr, "BAD: invalid args\n"); return 1; }
  //
  if ((hexstr = str2hex(NULL, argv[2])) == NULL) { fprintf(stderr, "BAD: invalid text\n"); return 1; }
  //
  msglen = strlen(hexstr)/*/2*/;
  msgmax = (msglen+MAXSMSLEN-1)/MAXSMSLEN;
  if (msgmax > 250) { free(hexstr); fprintf(stderr, "BAD: text too long\n"); return 1; }
  //
  fprintf(stderr, "sending %d message%s\n", msgmax, (msgmax == 1 ? "" : "s"));
  //
  res = pnptySendCommand(pd, "AT+CSCS=\"HEX\"");
  if (res == NULL || res[0] == 1) { free(hexstr); fprintf(stderr, "ERROR: CSCS\n"); goto error; }
  free(res);
  //
  // for delivery report to work the program should be listening until the message is delivered:
  //+CDS: 6,<the SMS ref num above>,,,<date & time>,<date & time>,0 or 48
  // last character - 0 being successful, 48 delivery failed
  // this is not possible with a CLI program (should exit as the sms is sent).
  //
  // validity period:
  //   0 - 143: (VP + 1) x 5 mins");
  // 144 - 167: 12 Hours + ((VP-143) x 30 mins)");
  // 168 - 196: (VP-166) x 1 day");
  // 197 - 255: (VP-192) x 1 week");
  // 167: one day
  //
  // 17: options (32 -- with delivery report?)
  // 167: validity
  // 8: dcs (24 for flash SMS) -- Data Coding Scheme
  // see http://www.dreamfabric.com/sms/dcs.html
  // 00 0 0 10 00: normal USC
  // 00 0 1 10 00: alert
  for (int f = 0; f < msgmax; ++f) {
    char msg[400];
    const char *mpos;
    int mlen;
    //
    // make UDHI
    memset(msg, 0, sizeof(msg));
    sprintf(msg, "050003%02X%02X%02X", msgid, msgmax, f+1);
    // append text part
    mpos = hexstr+f*MAXSMSLEN/**2*/;
    mlen = strlen(mpos);
    if (mlen > FULLSMSLEN) mlen = FULLSMSLEN;
    memcpy(msg+strlen(msg), mpos, mlen);
    //
    // 0: PDU mode; 1: text mode
    res = pnptySendCommand(pd, "AT+CMGF=1");
    if (res == NULL || res[0] == 1) { fprintf(stderr, "ERROR: CMGF\n"); goto error; }
    free(res);
    //
    res = pnptySendCommand(pd, "AT+CSMP=%d,%d,0,%d", 81, 167, 8);
    if (res == NULL || res[0] == 1) { fprintf(stderr, "ERROR: CSMP\n"); goto error; }
    free(res);
    res = NULL;
    //
    //fprintf(stderr, "[%s]\n", msg);
    if (pnptySendSMSCommand(pd, argv[1], msg) < 0) { fprintf(stderr, "ERROR: SMS\n"); goto error; }
  }
  //
  free(hexstr);
  return 0;
error:
  if (res != NULL) free(res);
  free(hexstr);
  fprintf(stderr, "BAD: some error occured.\n");
  return 1; // error
}


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  const char *name;
  const char *desc;
  int (*fn) (PnPty *pd, int argc, char *argv[]); // argv[0]: command name
  int minargs; // -1: any; 0: no args (i.e. argv[0] is not counted)
  int maxargs; // -1: any; 0: no args (i.e. argv[0] is not counted)
} CmdHandler;

static const CmdHandler commands[] = {
  {"help", "show all available commands with short descriptions", cmdHelp, 0, 0},
  {"me", "return phone number from SIM", cmdMe, 0, 0},
  {"rssi", "show signal power", cmdRSSI, 0, 0},
  //
  {"ussd", "send USSD query (provide full query, such as '*101#')", cmdUSSD, 1, 1},
  //
  {"balance-mts", "send MTS balance USSD query", cmdBalanceMTS, 0, 0},
  //
  {"command", "send AT command (for example '+CPIN?')", cmdCommand, 1, 1},
  //
  {"sms", "send SMS: number sms [number...]", cmdSMS, 2, 2},
};


////////////////////////////////////////////////////////////////////////////////
static int cmdHelp (PnPty *pd, int argc, char *argv[]) {
  int maxcnlen = 0;
  //
  if (pd != NULL) pnptyFree(pd);
  //
  for (size_t f = 0; f < sizeof(commands)/sizeof(CmdHandler); ++f) {
    int len = strlen(commands[f].name);
    //
    if (len > maxcnlen) maxcnlen = len;
  }
  //
  printf("command list:\n");
  for (size_t f = 0; f < sizeof(commands)/sizeof(CmdHandler); ++f) {
    int len = strlen(commands[f].name)-maxcnlen;
    //
    printf(" %s", commands[f].name);
    while (len-- > 0) fputc(' ', stdout);
    printf(" -- %s\n", commands[f].desc);
  }
  //
  exit(0);
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
static const CmdHandler *findCommand (const char *name) {
  if (name != NULL && name[0]) {
    for (size_t f = 0; f < sizeof(commands)/sizeof(CmdHandler); ++f) {
      if (strcasecmp(commands[f].name, name) == 0) return commands+f;
    }
  }
  //
  return NULL;
}


////////////////////////////////////////////////////////////////////////////////
int main (int argc, char *argv[]) {
  PnPty *pd;
  const CmdHandler *ch;
  int res;
  char *s;
  //
  if (argc < 2) cmdHelp(NULL, 0, NULL);
  //
  --argc;
  ++argv;
  //
  if ((ch = findCommand(argv[0])) == NULL) {
    fprintf(stderr, "FATAL: unknown command: '%s'!\n", argv[0]);
    return 1;
  }
  //
  if (ch->minargs >= 0 && argc-1 < ch->minargs) {
    fprintf(stderr, "FATAL: '%s' needs at least %d arguments!\n", argv[0], ch->minargs);
    return 1;
  }
  //
  if (ch->maxargs >= 0 && argc-1 > ch->maxargs) {
    if (ch->maxargs == 0) {
      fprintf(stderr, "FATAL: '%s' needs no arguments!\n", argv[0]);
    } else {
      fprintf(stderr, "FATAL: '%s' needs no more than %d arguments!\n", argv[0], ch->minargs);
    }
    return 1;
  }
  //
  if ((pd = pnptyNew()) == NULL) {
    fprintf(stderr, "FATAL: can't start pnatd!\n");
    return 1;
  }
  //
  // switch output encoding to GSM default encoding
  if ((s = pnptySendCommand(pd, "AT+CSCS=\"GSM\"")) == NULL || s[0] == '\1') {
    pnptyFree(pd);
    return 1;
  }
  free(s);
  //
  ussdd_skipnext();
  res = ch->fn(pd, argc, argv);
  ussdd_shownext();
  //
  pnptyFree(pd);
  //
  return res;
}
